(function($){
	$(function(){
		var $headings = $("table.responsive-table thead th");
		var $rows = $("table.responsive-table tbody tr");
		var $row, $cell;
		for (var i = 0; i < $rows.length; i++) {
			$row = $rows.eq(i);
			$cells = $row.children("td");
			for (var z = 0; z < $cells.length; z++) {
				$cell = $cells[z];
				$cell.prepend($headings[z].html());
			}
		}
		for (var i = 0; i < $headings.length; i++) {
			console.log($headings[i]);
		}
	});
})(jQuery);
