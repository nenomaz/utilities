<?php
class RowsGenerator {
	
	private $itemsNumberPerRow, $itemsTotal, $outputBeforeItem, $outputAfterItem;
	
	public function __construct($itemsNumberPerRow, $itemsTotal, $outputBeforeItem, $outputAfterItem) {
		$this->itemsNumberPerRow = $itemsNumberPerRow;
		$this->itemsTotal = $itemsTotal;
		$this->outputBeforeItem = $outputBeforeItem;
		$this->outputAfterItem = $outputAfterItem;
	}
	
	public function getOutputBeforeItem($itemIndex) {
		if ($this->isTheFirstOfTheRow($itemIndex))
			return $this->outputBeforeItem;
	}
	
	public function getOutputAfterItem($itemIndex) {
		if ($this->isTheLastOfTheRow($itemIndex))
			return $this->outputAfterItem;
	}
	
	private function isTheLastOfTheRow($itemIndex) {
	    return (($itemIndex + 1) % $this->itemsNumberPerRow) == 0 || ($itemIndex + 1) == $this->itemsTotal;
	}
	
	private function isTheFirstOfTheRow($itemIndex) {
		return ($itemIndex % $this->itemsNumberPerRow) == 0 || $itemIndex == 0;
	}
}
