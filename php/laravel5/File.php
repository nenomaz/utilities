<?php

namespace App\Http\Controllers\Components;

use Illuminate\Support\Facades\Storage;
use App\Exceptions\UserErrorException;

class File {

	// Stringhe per i tipi di file
	const TIPO_FILE_IMMAGINE = 'immagine';
	// Array di formati comunemente accettati, in base al tipo di file
	const FORMATI_GENERICI_ACCETTATI = [
		TIPO_FILE_IMMAGINE => ['jpeg', 'jpg', 'png', 'gif']
	];

	/* Salva un file nel percorso di destinazione specificato
	 * @percorsoImmagine il file proveniente dalla request
	 * @percorsoDestinazione il percorso di destinazione
	 * @return true in caso di successo, altrimenti false
	 */
	public static function salvaFile($file, $percorsoDestinazione, $opzioni=[]) {
		#self::controllaFile($file, $opzioni);
		if (Storage::makeDirectory($percorsoDestinazione)) {
			if ($file->move($percorsoDestinazione, $file->getClientOriginalName())) {
				return true;
			} else {
				throw new UserErrorException("Non � stato possibile salvare il file.");
			}
		} else {
			throw new UserErrorException("Non � stato possibile salvare il file.");
		}
	}

	public static function controllaFile($file, $opzioni=[]) {
		$estensioneFile = $file->getClientOriginalExtension();
		// File php non ammessi
		if ($estensioneFile == 'php') {
			throw new UserErrorException("Ops. Errore non previsto. Formato file non accettato");
		}
		// Se viene definito il tipo di file, utilizzando le costanti della classe, allora verranno ammessi i formati piùcomunemente diffusi per tale tipo di file
		if (!empty($opzioni['tipoFile'])) {
			if (!in_array($estensioneFile, self::FORMATI_GENERICI_ACCETTATI[$opzioni['tipoFile']]))
				throw new UserErrorException("Ops. Formato file non accettato.");
		}
		// Se sono definiti i formati accettati, allora avviene la verifica in base ad essi
		if (isset($opzioni['formatiAccettati'])) {
			if (!in_array($estensioneFile, $opzioni['formatiAccettati']))
				throw new UserErrorException("Ops. Formato file non accettato.");
		}
	}
}
