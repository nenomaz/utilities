
	function testUploadedImage($nome) {
		if (!is_uploaded_file($nome)) {
			throw new CakeException('L\'immagine non è stata caricata');
		}
		$information = getimagesize($nome);
		if (!$information) {
			throw new CakeException('Devi caricare un\'immagine valida');
		}
		$formato = $information[2];
		if (!($formato == IMG_GIF || $formato == IMG_JPEG || $formato == IMG_JPG || $formato == IMG_PNG)) {
			throw new CakeException('L\'immagine caricata dev\'essere del formato GIF, JPEG o PNG');
		}
		return true;
	}

	function testGoogleRecaptcha($recaptcharesponse) {
		$secret; // TODO: SECRET KEY
		$clientip; // TODO: CLIENT IP
		$data = array(
			'secret' => $secret,
			'response' => $recaptcharesponse,
			'clientip' => $clientip
		);
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
		$response = json_decode($response);
		return $response->success;
	}
